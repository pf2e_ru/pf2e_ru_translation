.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Пайрака (Pairaka)
============================================================================================================

Часто очаровательные, иногда даже соблазнительные, пайраки проникают в отношения смертных, тонко разрушая дружеские и любовные узы через эмоциональное и физическое развращение и чуму.
Пайраки выглядят как красивые гуманоиды с голубой кожей, покрытые крупной сыпью и нарывами, но они редко принимают свою истинную форму.
Вместо этого они принимают облик, который привлекает достаточно внимания, чтобы они могли проникнуть в доверие к тем, кого они хотят мучить и развращать.



.. rst-class:: creature
.. _bestiary--Pairaka:

Пайрака (`Pairaka <https://2e.aonprd.com/Monsters.aspx?ID=1119>`_) / Существо 7
------------------------------------------------------------------------------------------------------------

- :alignment:`ПЗ`
- :size:`средний`
- див
- бес

**Источник**: Bestiary 3 pg. 70

**Восприятие**: +15;
:ref:`сильное ночное зрение <cr_ability--Darkvision>`

**Языки**: Всеобщий,
Дэймонический;
:ref:`телепатия <cr_ability--Telepathy>` 100 футов

**Навыки**:
Акробатика +14,
Запугивание +16,
Обман +20,
Скрытность +16,
Дипломатия +20,
Общество +13,
Аркана +13,
Религия +13

**Сил** +3,
**Лвк** +5,
**Тел** +3,
**Инт** +2,
**Мдр** +4,
**Хар** +7

----------

**КБ**: 24;
**Стойкость**: +12,
**Рефлекс**: +16,
**Воля**: +17;
+1 состояния ко всем спасброскам от магии

**ОЗ**: 105

**Иммунитеты**: :t_disease:`болезнь`

**Слабости**:
:ref:`холодное железо <material--Cold-Iron>` 5,
добро 5


**Ненависть к красному (Hatred of Red)**: Пайраки ненавидят красный цвет.
Они не надевают его и не хотят заходить в любое место, окрашенное в красный.
Если у них будет выбор, то они первыми нападут на существо в красном.
Если им препятствуют выражать свое недовольство этим цветом с помощью силы или какого-то магического эффекта, они в конце своего хода получают 2d6 ментального урона.

----------

**Скорость**: 25 футов,
полет 35 футов


**Ближний бой**: |д-1| коготь +16 [+12/+8] (:w_agile:`быстрое`, :w_finesse:`точное`, :t_evil:`зло`, :t_magical:`магический`),
**Урон** 2d8+6 рубящий + 1d6 зло и :ref:`disease--Bubonic-Plague` (КС 23)


**Врожденные сакральные заклинания** КС 25

| **4 ур.**
| :ref:`spell--c--Charm` (по желанию)
| :ref:`spell--d--Dimension-Door` (по желанию)
| :ref:`spell--m--Misdirection` (по желанию, только на себя)
| :ref:`spell--o--Outcasts-Curse` (по желанию)
| :ref:`spell--s--Suggestion` (по желанию)
| **Чары (4 ур.)** :ref:`spell--d--Detect-Magic`

**Ритуалы** КС 25; :ref:`ritual--Div-Pact`


:ref:`cr_ability--Change-Shape` |д-1|
(:t_divine:`сакральный`, :t_transmutation:`трансмутация`, :t_polymorph:`полиморф`, :t_concentrate:`концентрация`)
Пайрака может принять внешний вид любого :s_small:`маленького` или :s_medium:`среднего` :t_humanoid:`гуманоида` или :t_animal:`животного`.
Это не изменяет Скорость, либо модификаторы атак и урона Ударов, но может изменить тип урона наносимый Ударами (обычно на дробящий).


**Мучительные сны (Tormenting Dreams)** |д-2|
(:t_divine:`сакральный`, :t_enchantment:`очарование`, :t_emotion:`эмоция`, :t_mental:`ментальный`)
**Частота**: раз в день;
**Эффект**: Пайрака мучает спящее существо в пределах 100 футов видениями предательства друзей и близких.
Цель должна совершить спасбросок Воли КС 25 с эффектами заклинания :ref:`spell--n--Nightmare`.


**Бубонная чума (Bubonic Plague)**
(:t_disease:`болезнь`)
Существо не может убрать состояние :c_fatigued:`утомления`, пока поражено данной болезнью.

| **Спасбросок**: Стойкость КС 23
| **Возникновение**: 1 день
| **Стадия 1**: :c_fatigued:`утомление` (1 день)
| **Стадия 2**: :c_enfeebled:`ослаблен 2` и :c_fatigued:`утомление` (1 день)
| **Стадия 3**: :c_enfeebled:`ослаблен 3`, :c_fatigued:`утомление` и получаете 1d6 продолжительного урона кровотечением каждые 1d20 минут (1 день)





.. include:: /helpers/actions.rst