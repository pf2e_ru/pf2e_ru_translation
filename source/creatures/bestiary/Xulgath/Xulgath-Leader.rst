.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Зулгат-лидер (Xulgath Leader)
============================================================================================================

Лидерами зулгатов обычно становятся самые сильные и жестокие представители общины, хотя в некоторых случаях, особенно в больших группах, племена зулгатов возглавляют могущественные культисты демонов или других монстров, которые стремятся использовать зулгатов в качестве слуг или рабов.



.. rst-class:: creature
.. _bestiary--Xulgath-Leader:

Зулгат-лидер (`Xulgath Leader <https://2e.aonprd.com/Monsters.aspx?ID=420>`_) / Существо 3
------------------------------------------------------------------------------------------------------------

- :alignment:`ХЗ`
- :size:`средний`
- зулгат
- гуманоид

**Источник**: Bestiary pg. 337

**Восприятие**: +9;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Драконий,
Подземный

**Навыки**:
Атлетика +11,
Запугивание +6,
Скрытность +6

**Сил** +4,
**Лвк** +1,
**Тел** +2,
**Инт** -1,
**Мдр** +2,
**Хар** +1


**Предметы**:
двуручный топор,
метательное копье (4),
нагрудник

----------

**КБ**: 20;
**Стойкость**: +9,
**Рефлекс**: +6,
**Воля**: +9

**ОЗ**: 44


**Вонь (Stench)**
(:t_aura:`аура`, :t_olfactory:`обонятельный`)
30 футов.
Существо попадающее в эту область, должно совершить спасбросок Стойкости КС 19.
При провале, оно испытывает :c_sickened:`тошноту 1`, а при критическом провале, оно еще получает штраф состояния -5 футов к своим Скоростям на 1 раунд.
Находясь в этой ауре, существо получает штраф обстоятельства -2 к спасброскам для восстановления от этого состояния :c_sickened:`тошноты`.
Существо добившееся успешного спасброска становится временно иммунно к вони всех :t_xulgath:`зулгатов` на 1 минуту.

----------

**Скорость**: 25 футов


**Ближний бой**: |д-1| двуручный топор +11 [+6/+1] (:w_sweep:`размах`),
**Урон** 1d10+6 рубящий + ослабляющий удар

**Ближний бой**: |д-1| челюсти +11 [+6/+1],
**Урон** 1d6+6 колющий + ослабляющий удар

**Ближний бой**: |д-1| коготь +11 [+7/+3] (:w_agile:`быстрое`),
**Урон** 1d4+6 рубящий


**Дальний бой**: |д-1| метательное копье +8 [+3/-2] (:w_thrown:`метательное 30 фт`),
**Урон** 1d6+4 колющий


**Ослабляющий удар (Weakening Strike)** |д-1|
Цель должна совершить успешный спасбросок Стойкости КС 20, иначе будет :c_enfeebled:`ослаблена 1` (при крит.провале :c_enfeebled:`ослаблена 2`) на 1 раунд.





.. include:: /helpers/actions.rst