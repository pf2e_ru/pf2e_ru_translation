.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Адукайт (Adhukait)
============================================================================================================

Вдвое более дисциплинированней монаха, и вдвое смертоносней воина, адукайт - это высшая порочность двойственности, два шипастых воина-асура, насаженные друг на друга, образуют единую свирепую, невероятно эффективную машину для убийства.
Две головы, два сердца и два разума адукайта редко работают как единое целое - разве что для того, чтобы калечить и убивать.

Даже происхождение адукайтов это не одна, а две истории.
Одни считают, что они появились, когда два богоподобных разбойника попытались совершить налет на небесный зал, и их тела были раздавлены и сброшены с небес.
Согласно другой легенде, два близнеца-асура однажды вызвали на поединок великого воина, чтобы тот по очереди сразился с ними.
Когда они напали на него вдвоем, воин был в такой ярости, что шмякнул одного о другого с такой силой, что с того дня они срослись.

Адукайты - одни из самых элитных воинов-асуров, восполняющие свой недостаток заклинательства злобностью и неотступным стремлением причинять боль.



.. rst-class:: creature
.. _bestiary--Adhukait:

Адукайт (`Adhukait <https://2e.aonprd.com/Monsters.aspx?ID=1070>`_) / Существо 7
------------------------------------------------------------------------------------------------------------

- :alignment:`ПЗ`
- :size:`средний`
- асура
- бес

**Источник**: Bestiary 3 pg. 22

**Восприятие**: +15;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Всеобщий,
Инфернальный;
:ref:`телепатия <cr_ability--Telepathy>` 30 футов

**Навыки**:
Атлетика +19,
Акробатика +15,
Запугивание +15,
Скрытность +15,
Выступление +15

**Сил** +6,
**Лвк** +4,
**Тел** +4,
**Инт** +2,
**Мдр** +2,
**Хар** +4


**Предметы**:
кукри (4)

----------

**КБ**: 25, :ref:`круговое зрение <cr_ability--All-Around-Vision>`;
**Стойкость**: +15,
**Рефлекс**: +17,
**Воля**: +13

**ОЗ**: 130

**Иммунитеты**: :t_curse:`проклятие`

**Слабости**: добро 5


**Двойной разум (Dual Mind)** |д-р|
**Триггер**: Адукайт проваливает спасбросок против :t_mental:`ментального` эффекта;
**Эффект**: Адукайт переводит эффект в один из своих разумов, делая его временно бесчувственным.
Он меняет свой результат на успех, но одно из его тел повисает без движения до конца своего следующего хода.
На это время адукайт становится :c_clumsy:`неуклюжим 2`; получает штраф обстоятельства -10 футов к Скорости; и не может использовать "Двойной разум", "Двойную возможность" или "Танец разрушения".


:ref:`cr_ability--Attack-of-Opportunity` |д-р|

**Двойная возможность (Dual Opportunity)**: Адукайт каждый раунд получает вторую реакцию, которую может использовать только на :ref:`cr_ability--Attack-of-Opportunity`.

----------

**Скорость**: 40 футов


**Ближний бой**: |д-1| кукри +18 [+14/+10] (:w_agile:`быстрое`, :w_trip:`опрокидывание`),
**Урон** 1d6+9 рубящий + 2d6 продолжительный кровотечением и 1d4 зло

**Ближний бой**: |д-1| коготь +18 [+14/+10] (:w_agile:`быстрое`),
**Урон** 1d6+9 рубящий и 1d4 зло


**Танец разрушения (Dance of Destruction)** |д-1|
**Требования**: Последним действием адукайта был Удар нанесший урон;
**Эффект**: Адукайт :ref:`Перемещается (Strides) <action--Stride>` вплоть до 10 футов и наносит Удар.





.. include:: /helpers/actions.rst