
.. _LO--Impossible-Lands:

Невозможные Земли (Lost Omens: Impossible Lands)
============================================================================================================

**Источник**: Lost Omens: Impossible Lands

Здесь собран перечень элементов правил появившихся в книге "Lost Omens: Impossible Lands" и структурирован по регионам так же, как и в оригинале.

.. toctree::
   :maxdepth: 2

   Alkenstar
   Bhopan
   Geb
   Jalmeray
   The-Mana-Wastes
   Nex
