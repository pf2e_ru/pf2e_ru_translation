.. include:: /helpers/roles.rst

.. _SoM:

Секреты магии (`Secrets of Magic <https://2e.aonprd.com/Rules.aspx?ID=1511>`_)
============================================================================================================

**Источник**: Secrets of Magic pg. 193

В "Секретах магии" представлены 2 новых класса.

* :doc:`/classes/magus` (+ архетип :doc:`Магуса </classes/archetypes/magus>`)
* :doc:`/classes/summoner` (+ архетип :doc:`Призывателя </classes/archetypes/summoner>`)


Далее представлен широкий спектр необычных магических практик и опций для персонажей, многие из которых являются :r_uncommon:`необычными` или :r_rare:`редкими`.
Добавив их в свою игру, вы сможете расширить и обогатить повествование о том, как работает магия.

.. toctree::
   :caption: Книга безграничной магии (Book of Unlimited Magic)
   :maxdepth: 2

   Cathartic-Magic
   Flexible-Preparation
   Elementalism
   Geomancy
   Shadow-Magic
   Soulforged-Armaments
   Thassilonian-Rune-Magic
   Wellspring-Magic
   True-Names
   Ley-Lines
   Pervasive-Magic
