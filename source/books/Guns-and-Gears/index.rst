
.. _Guns-and-Gears:

Пушки и Шестерни (`Guns and Gears Introduction <https://2e.aonprd.com/Rules.aspx?ID=1587>`_)
============================================================================================================

.. epigraph::

   *Ролевая игра Pathfinder представляет собой сеттинг высокого фэнтези, в котором можно рассказать почти любую историю.
   Если "Основная книга правил" предоставляет классические опции игры, а "Секреты магии" дают персонажам варианты для более глубокого погружения в магические мистерии Голариона, то в "Пушках и Шестернях" представлены секреты оружия на черном порохе и технологии заводных механизмов, сочетающие в себе скрип и стимпанк, что придает игре особый колорит.*

-----------------------------------------------------------------------------

**Источник**: Guns & Gears pg. 5

.. toctree::
   :maxdepth: 2

   Guns-and-Gears
   Gears-characters
   Gears-equipment
   Guns-characters
   Guns-equipment
