.. include:: /helpers/roles.rst

.. rst-class:: ancestry
.. _ancestry--Nagaji:

Нагаджи (`Nagaji <https://2e.aonprd.com/Ancestries.aspx?ID=54>`_)
=============================================================================================================

.. epigraph::
	
	*Обладая гуманоидными телами и змеевидными головами, нагаджи являются глашатаями, спутниками и слугами могущественных наг.
	Они с глубоким почтением относятся к святым местам и духовным истинам, что многие другие считают столь же пугающим, как и внешность нагаджи.*

-----------------------------------------------------------------------------

.. rst-class:: sidebar-char-ancestry-class

.. sidebar:: hidden

	.. rubric:: Источник

	Lost Omens: Impossible Lands pg. 47


	.. rubric:: Редкость

	.. rst-class:: traits

	- :uncommon:`необычный`


	.. rubric:: Очки здоровья

	10


	.. rubric:: Размер

	:s_medium:`Средний`


	.. rubric:: Скорость

	25 футов


	.. rubric:: Повышения характеристик

	Сила

	Свободное


	.. rubric:: Языки

	Всеобщий

	Нагаджийский

	Дополнительные языки в количестве вашего модификатора Интеллекта (если положительный).
	Выберите из Акло, Амуррунский, Акван, Небесный, Драконий, Подземный, Теневой, Тэнгу, Ванарский и тех к которым у вас есть доступ (распространенные в вашем регионе).


	.. rubric:: Признаки

	:t_nagaji:`Нагаджи`

	:t_humanoid:`Гуманоид`


	.. rubric:: Сумеречное зрение

	Вы можете видеть при тусклом свете, как если бы это был яркий свет, и вы игнорируете состояние :c_concealed:`скрыт` из-за тусклого света.


	.. rubric:: Клыки (Fangs)

	У вас во рту либо ряды крючковатых, острых как иглы зубов, либо пара опасных змеиных клыков.
	Вы получаете безоружную атаку клыками, которая наносит 1d6 колющего урона.
	Ваши клыки относятся к группе оружия ":ref:`драка <weapon--crit-spec--Brawling>`" и имеют признаки :w_finesse:`точное` и :w_unarmed:`безоружное`.




Нагаджи - творения богини :doc:`/lost_omens/Deity/Tian-God/Nalinivati`, которая вдохновлялась как людьми, так и :doc:`нагами </creatures/bestiary/Naga/index>`.
Это вдохновение, наряду с преданностью нагаджи нагам, заставило многих утверждать, что нагаджи были созданы для того, чтобы быть слугами.
Однако Налинивати создала нагаджи просто ради акта созидания.
Она представляла себе мир, где наги и нагаджи трудятся вместе для достижения успеха, где наги служат священными хранителями, а нагаджи живут на Голарионе как смертные, уважая наг за их силу и мудрость.
Нагаджи оправдывают эти ожидания, основывая нации, храмы и деревни с таким количеством разнообразных правительств, обществ и традиций, сколько чешуек на спине змея.
Если о нагаджи и можно сказать, что у них есть какая-то общая черта, то это преданность, будь то общине, храму, концепции или образу жизни.
Большинство нагаджи также тяготеют к духовности во всех ее проявлениях, даже к темным аспектам философий и религий.

Если вам нужен персонаж, сочетающий в себе сокрушительную силу и извилистую таинственность змея, вам стоит сыграть за нагаджи.



.. rst-class:: h3
.. rubric:: Вы можете ...

* Наслаждаться пересечением духовности и повседневной жизни.
* Обладать большим терпением для выполнения задач, которые другим могут показаться скучными.
* Придерживаться сильных убеждений и традиций относительно своего места во Вселенной.


.. rst-class:: h3
.. rubric:: Другие возможно ...

* Предполагают, что у вас нет никаких целей или интересов, кроме служения нагам.
* Считают пугающими ваши немигающие глаза и змеиные черты лица.
* Потрясены вашей связью и преданностью святым природным местам.



.. rst-class:: h3
.. rubric:: Физическое описание

Нагаджи склонны к резким отличиям во внешности.
У некоторых из них есть хвосты и ноги, у некоторых - острые когти на руках и ногах, а некоторые настолько отличаются от своих сородичей, что их принимают за :doc:`ламий </creatures/bestiary/Lamia/index>`, а не за нагаджи.
Наиболее характерные черты нагаджи - змеиная голова и гуманоидное тело.
Обычно у них немигающие змеиные глаза, хотя другие народы часто утверждают, что взгляд нагаджи гораздо более пристальный.
Тело нагаджи покрыто чешуей, имеющей узоры подобные тем, что есть у змей или наг.
В зависимости от наследия, шея нагаджи может быть длинной, как у лебедя, или короткой, как у человека, а у многих из них к спине спускается капюшон из кожи или чешуи.
Как правило, нагаджи, обладают сокрушительной силой, но ее проявление может различаться; у одних громоздкое телосложение, у других - стройная, но мощная мускулатура змеи.



.. rst-class:: h3
.. rubric:: Общество

Общины нагаджи очень разнообразны - от древних империй до крошечных рыбацких деревушек.
Обычно они, изолированы от других народов, но скорее из соображений удобства, чем по собственному желанию; физические потребности нагаджи отличаются от потребностей большинства родословных, поэтому, даже интегрируясь в смешанные сообщества, они, как правило, живут с другими нагаджи.
В конкретных общинах нагаджи отличается все, начиная от брачных традиций, религии, социальных ролей, ценного искусства и методов управления.
Однако, внутри этих сообществ, нагаджи зачастую придерживаются очень строгих и традиционных взглядов на данные темы.
Многочисленные случаи войн между нагаджи возникали из-за того, что одна группа нагаджи считала неприемлемой практику другой.
Эта точка зрения распространяется и на отношения с другими народами.
Например, большинство нагаджи считают себя соперниками и врагами :doc:`гаруд </creatures/bestiary/Garuda>` из-за их исторических легенд, даже если нагаджи никогда не видели гаруду.

Нагаджи зародились в Нагаджоре - регионе Тянь Ся.
Хотя с тех пор они распространились по Голариону в такие регионы, как Вудра и Джалмерэй, многие из них по-прежнему следуют традициям своей родины.
Среди нагаджи очень распространены тяньские и вудранские представления о стихиях, медицине и духовности, особенно учитывая их склонность считать себя очень благочестивыми существами.
Нагаджи в основном отвергают человеческую концепцию каст, не считая смутного представления о "естественном порядке", в котором наги считаются святыми проводниками смертных нагаджи, но они могут видеть привлекательность концепции кармы и праведности выполнения своей роли во вселенной.
Хотя физическая потребность в одежде у них невелика, поскольку они придерживаются жаркой и влажной окружающей среды, нагаджи носят ее с гордостью и относятся к ней как к декоративным элементам, таким же, как украшения.

Поскольку у наг обычно матриархальный строй, нагаджи с большим уважением относятся к женщинам, занимающим руководящие должности.
Не все нагаджи следуют примеру матриархальных обществ, но они склонны вести родословную по материнской линии, и почти ни одно общество нагаджи не является строго патриархальным.



.. rst-class:: h3
.. rubric:: Мировоззрение и религия

Нагаджи чаще всего нейтральны, но в остальном не испытывают сильной тяги к определенным мировоззрениям.
Присутствие наги может склонить нагаджи к этике этой наги, но взгляды нагаджи на жизнь, как правило, определяются культурой, к которой они принадлежат.

В подавляющем большинстве, нагаджи поклоняются :doc:`/lost_omens/Deity/Tian-God/Nalinivati` - своей создательнице, поэтому любые нагаджи, которые воздают должное другому богу, а не Налинивати, вызывают у своих сородичей сильную негативную реакцию.
Хотя нагаджи считают почитание :doc:`Равитры </lost_omens/Deity/Other/More/Ravithra>` (предполагаемой матери наг) допустимым и принимают его охотнее, чем другую веру, большинство считает, что Равитру не следует беспокоить просьбами смертных последователей.
Очень небольшое меньшинство нагаджи в Джалмерэе поклоняются богу войны :doc:`Диомазулу </lost_omens/Deity/Vudrani-God/Diomazul>`; другие нагаджи относятся к ним терпимо, но считают их такими же страшными и опасными, как и их бог-покровитель, и обычно избегают их.



.. rst-class:: h3
.. rubric:: Нагаджи-авантюристы

Будучи костяком общества управляемого нагами, нагаджи могут иметь любую предысторию, которую только можно себе представить, отражая их предполагаемую роль в обществе.
За пределами царства наг, нагаджи-авантюристы часто становятся :ref:`послушниками <bg--Acolyte>`, :ref:`эмиссарами <bg--Emissary>`, :ref:`стражниками <bg--Guard>` или :ref:`купцами <bg--Merchant>`.
Сила нагаджи означает, что они превосходны как :doc:`воины </classes/fighter>` и :doc:`чемпионы </classes/champion>`, но они достаточно гибкие, чтобы успешно справляться с любой ролью, к которой они стремятся, и популярными профессиями являются :doc:`жрецы </classes/cleric>`, :doc:`чародеи </classes/sorcerer>` и :doc:`алхимики </classes/alchemist>`.

Чаще всего нагаджи отправляются в приключения по приказу старшей наги или ради блага общины нагаджи, но их причины могут быть такими же разнообразными, как и у других народов Голариона.
Нагаджи могут отправиться в мир на поиски святых мест, в поисках просветления, материальных сокровищ или по личным причинам, связанным с их предысторией, семьей или городом.



.. rst-class:: h3
.. rubric:: Имена

Имена нагаджи варьируются в зависимости от того, в каком регионе мира они живут, но они, как правило, имеют краткие гласные, если таковые вообще имеются.
В детстве нагаджи обычно сами дают себе имя или позволяют группам сверстников дать имена друг другу, и эта традиция часто приводит к тому, что нагаджи охотно меняют эти имена на новые, когда достигают зрелого возраста.

.. rst-class:: h4
.. rubric:: Пример имен

Адэша, Гариджа, Кая, Кувана, Онок, Паравата, Шени, Такаша, Таси, Улу, Васки, Юльбин



.. rst-class:: h3
.. rubric:: Другая информация

.. rst-class:: h4
.. rubric:: Анклавы нагаджи (Nagaji Enclaves)

**Источник**: Lost Omens: Impossible Lands pg. 50

Родина нагаджи - Нагаджор, жаркая и покрытая джунглями земля на юге континента Тянь Ся.
Здесь господствуют государства наг-правителей и граждан нагаджи, каждое из которых уникально само по себе; налево пойдешь - можешь прийти к властной тирании, а направо пойдешь - в идиллический рай попадешь.
Однако везде, где можно встретить наг, туда за ними быстро следуют нагаджи.
Поселения нагаджи также встречаются в Вудре, центральном и южном Тянь Ся, и Джалмерэе, а некоторые из них проникают в Гарунд и Касмарон.


.. rst-class:: h4
.. rubric:: Нагаджи-путешественники (Nagaji Travelers)

**Источник**: Lost Omens: Impossible Lands pg. 51

Нагаджи очень редко встречаются вне жаркого и влажного климата.
Их змееподобная натура делает их вялыми в холодную погоду, и им требуется определенное количество влаги, чтобы легко линять - чрезмерная сухость может привести к тому, что омертвевшая кожа неприятно прилипает к их чешуе.
В областях, где с нагаджи незнакомы, их также могут принять за :doc:`змеелюдов </creatures/bestiary/Serpentfolk/index>`, что является трагическим недоразумением, которое не дает сообществам нагаджи распространяться дальше.






Наследия нагаджи (Nagaji Heritages)
-----------------------------------------------------------------------------------------------------------

Обычно физиология нагаджи различается от индивида к индивиду.
Выберите одно из следующих наследий на 1-м уровне.


.. _ancestry-heritage--Nagaji--Hooded:

С капюшоном (`Hooded Nagaji <https://2e.aonprd.com/Heritages.aspx?ID=216>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Impossible Lands pg. 48

У вас голова плюющейся кобры, покрытая капюшоном, и, подобно таким кобрам, можете выпускать из пасти струи яда.
Вы получаете безоружную дистанционную атаку ядовитого плевка, имеющую шаг дистанции 10 футов и наносящую 1d4 урона ядом.
При критическом попадании, цель получает продолжительный урон ядом, равный количеству костей урона оружия.
У вашего плевка нет группы оружия или эффекта критической специализации.


.. _ancestry-heritage--Nagaji--Sacred:

Священный (`Sacred Nagaji <https://2e.aonprd.com/Heritages.aspx?ID=217>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Impossible Lands pg. 48

Вы отличаетесь от большинства нагаджи - ваша верхняя часть тела красивая человеческая, а нижняя - зеленой или белой змеи.
Легенды утверждают, что вашими предками были не нагаджи созданные :doc:`/lost_omens/Deity/Tian-God/Nalinivati`, а верные змеи, вознесенные богиней.
Вместо безоружной атаки клыками у вас есть атака хвостом, которая наносит 1d6 дробящего урона, относится к группе оружия ":ref:`драка <weapon--crit-spec--Brawling>`" и имеет признаки :w_finesse:`точное` и :w_unarmed:`безоружное`.
Вы получаете бонус обстоятельства +2 к вашим КС Стойкости или Рефлекса против попыток :ref:`skill--Athletics--Grapple` или :ref:`skill--Athletics--Trip` вас.
Этот бонус также применяется к спасброскам против эффектов, которые :c_grabbed:`схватят`, :c_restrained:`сдержат` или собьют вас :c_prone:`ничком`.


.. _ancestry-heritage--Nagaji--Titan:

Титанический (`Titan Nagaji <https://2e.aonprd.com/Heritages.aspx?ID=218>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Impossible Lands pg. 48

Вы росли воином или телохранителем, и ваше особое питание и накачанные мышцы сделали вашу чешую прочной, как пластины доспеха.


Ваша чешуя является средним доспехом из группы :ref:`латных <ch6--Armor-Spec-Effects--Plate>`, дающая бонус предмета +4 к КБ, и ограничение модификатора Ловкости к КБ равное +1, штраф проверки -2, штраф Скорости -5 футов, значение Силы 16 и признак :a_comfort:`удобный`.
Вы никогда не сможете надевать другие доспехи или снять чешую.
Вы можете гравировать на чешую руны доспехов.


.. _ancestry-heritage--Nagaji--Venomshield:

Ядостойкий (`Venomshield Nagaji <https://2e.aonprd.com/Heritages.aspx?ID=219>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Impossible Lands pg. 48

Ваша внутренняя связь с нагами и обычными змеями дает вам врожденное сопротивление к токсинам любого вида.
Вы получаете сопротивление яду, равное половине вашего уровня (минимальное сопротивление 1), и получаете бонус обстоятельства +1 ко всем спасброскам против :t_poison:`яда`.


.. _ancestry-heritage--Nagaji--Whipfang:

Длинношеий (`Whipfang Nagaji <https://2e.aonprd.com/Heritages.aspx?ID=220>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Источник**: Lost Omens: Impossible Lands pg. 48

У вас длинная, гибкая шея, которая может поразительно скручиваться, как змея.
Ваши обманчиво мощные мышцы позволяют вам кусать с неожиданного расстояния и удивительной скоростью.
Вы получаете действие :ref:`action--Nagaji--Raise-Neck`.

.. rst-class:: description
.. _action--Nagaji--Raise-Neck:

Поднять шею (`Raise Neck <https://2e.aonprd.com/Actions.aspx?ID=1450>`_) |д-1|
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

**Источник**: Lost Omens: Impossible Lands pg. 48

Вы поднимаете свою голову в позицию для удара.
Удар клыками, полученный от родословной нагаджи, получает досягаемость 10 футов до конца вашего хода.




Универсальные наследия (Versatile Heritages)
-----------------------------------------------------------------------------------------------------------

.. include:: /helpers/versatile_heritages.rst










.. rst-class:: ancestry-class-feats

Способности родословной (Nagaji Ancestry Feats)
-----------------------------------------------------------------------------------------------------------

На 1-м уровне вы получаете одну способность родословной, и получаете дополнительную каждые 4 уровня после этого (на 5-м, 9-м, 13-м и 17-м уровнях).
Как нагаджи, вы выбираете из следующих способностей.


1-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Nagaji--Cold-Minded:

Сдержанный (`Cold Minded <https://2e.aonprd.com/Feats.aspx?ID=3982>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 48

----------

Тонкие нити манящей магии не оказывают на ваш разум никакого впечатления.
Вы получаете бонус обстоятельства +1 к спасброскам против эффектов :t_emotion:`эмоций`, и каждый раз, когда вы при броске спасброска против эффекта :t_emotion:`эмоций` получаете успеха, то вместо этого получаете критический успех.


.. _ancestry-feat--Nagaji--Lore:

Знания нагаджи (`Nagaji Lore <https://2e.aonprd.com/Feats.aspx?ID=3983>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Вы вассал или ученик учёной наги, и вы изучили секреты магии наг и приготовления сложных ядов.
Вы становитесь обучены Ремеслу и Оккультизму.
Если вы автоматически становитесь обучены одному из этих навыков (например, благодаря своей предыстории или классу), то вместо этого становитесь обучены навыку на свой выбор.
Еще вы становитесь обучены по своему выбору Знаниям нагаджи или Знаниям наг.


.. _ancestry-feat--Nagaji--Spell-Familiarity:

Знакомство с магией нагаджи (`Nagaji Spell Familiarity <https://2e.aonprd.com/Feats.aspx?ID=3984>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Из-за обучения, воздействия или семейной посвященности, в вашей крови бурлит магия наг.
Во время своих ежедневных приготовлений выберите :ref:`spell--d--Daze`, :ref:`spell--d--Detect-Magic` или :ref:`spell--m--Mage-Hand`.
До следующих ежедневных приготовлений вы можете творить выбранные чары как оккультное врожденное заклинание.
Чары усиливаются до уровня заклинания, равного половине вашего уровня, округленного до большего целого.


.. _ancestry-feat--Nagaji--Serpents-Tongue:

Язык змеи (`Serpent's Tongue <https://2e.aonprd.com/Feats.aspx?ID=3985>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Вы подсознательно водите языком по воздуху, пробуя окружающий мир на вкус.
Вы получаете неточный нюх с дальностью 30 футов.


.. _ancestry-feat--Nagaji--Water-Nagaji:

Водный нагаджи (`Water Nagaji <https://2e.aonprd.com/Feats.aspx?ID=3986>`_) / 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Подобно водной наге, вы установили связь со священным или нетронутым водоемом, либо в качестве дома, либо как с местом, которое нужно защищать.
Вы получаете бонусную общую способность :ref:`feat--Breath-Control` и Скорость плавания 10 футов.





5-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Nagaji--Hypnotic-Lure:

Гипнотическое приманивание (`Hypnotic Lure <https://2e.aonprd.com/Feats.aspx?ID=3987>`_) |д-2| / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- оккультный
- очарование
- визуальный
- ментальный
- концентрация
- нагаджи

**Частота**: раз в час

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Ваш немигающий взгляд настолько интенсивен, что может сбить с толку разум других, притягивая к вам жертву даже вопреки ее здравому смыслу.
Вы пристально смотрите на существо в пределах 30 футов.
Цель должна совершить спасбросок Воли против вашего КС класса или КС заклинания, в зависимости от того, что больше.

| **Успех**: Цель невредима.
| **Провал**: В свой ход цель должна потратить свое первое действие, чтобы приблизиться к вам. Она не может :ref:`action--Delay` или использовать реакции, пока не сделает это.
| **Критический провал**: В свой ход цель должна потратить все свои действия, чтобы приблизиться к вам. Она не может :ref:`action--Delay` или использовать реакции, пока не достигнет пространства находящегося рядом с вами (или так близко к вам как только возможно, если она достигает непроходимой преграды).


.. _ancestry-feat--Nagaji--Spell Mysteries:

Таинства магии нагаджи (`Nagaji Spell Mysteries <https://2e.aonprd.com/Feats.aspx?ID=3988>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Предварительные условия**: Хотя бы 1 врожденное заклинание от наследия нагаджи или способности родословной

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Вы изучили больше магии наг.
Во время своих ежедневных приготовлений выберите :ref:`spell--c--Charm`, :ref:`spell--f--Fleet-Step` или :ref:`spell--h--Heal`.
Вы можете раз в день творить его как врожденное оккультное заклинание 1-го уровня.


.. _ancestry-feat--Nagaji--Skin-Split:

Разрывание кожи (`Skin Split <https://2e.aonprd.com/Feats.aspx?ID=3989>`_) |д-2| / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Частота**: раз в день

**Источник**: Lost Omens: Impossible Lands pg. 49

----------

Вы вскрываете верхний слой своей чешуи и счищаете преждевременную линьку, чтобы удалить со своей кожи вредные вещества.
Вы мгновенно прекращаете весь продолжительный урон от эффектов, покрывающих вашу кожу (например, огонь и большинство продолжительного урона кислотной).
Если вы страдаете от эффекта, отличного от продолжительного урона, который зависит от постоянного контакта с вашей кожей, и если этот эффект позволяет спасбросок, то немедленно совершите новый спасбросок против этого эффекта.


.. _ancestry-feat--Nagaji--Venom-Spit:

Плевок ядом (`Venom Spit <https://2e.aonprd.com/Feats.aspx?ID=3990>`_) / 5
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 50

----------

Вы научились искусству метания ядовитых плевков в уязвимые места противника, особенно в глаза.
Вы получаете безоружную дистанционную атаку ядовитого плевка, имеющую шаг дистанции 10 футов и наносящую 1d4 урона ядом.
При критическом попадании, цель получает продолжительный урон ядом, равный количеству костей урона оружия.
У вашего плевка нет группы оружия или эффекта критической специализации.

**Особенность**: Если вы обладаете наследием нагаджи :ref:`ancestry-heritage--Nagaji--Hooded`, то в дополнение к вашему нормальному эффекту критического попадания ядовитого плевка, цель еще :c_dazzled:`ослеплена` до начала вашего следующего хода.





9-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Nagaji--Envenom-Strike:

Отравление удара (`Envenom Strike <https://2e.aonprd.com/Feats.aspx?ID=3991>`_) |д-1| / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Предварительные условия**: Вы обладаете Ударом, наносящим урон ядом, от наследия нагаджи или способности родословной

**Частота**: раз в 10 минут

**Источник**: Lost Omens: Impossible Lands pg. 50

----------

Вы выплевываете яд на оружие, которое вы держите в руках, или на оружие, которое держит готовое существо в пределах 30 футов; еще вы можете использовать это умение для отравления ядом своей безоружной атаки клыками нагаджи.
Если до начала вашего следующего хода, следующий :ref:`action--Strike` выбранным оружием попадает и наносит урон, то этот Удар наносит дополнительные 2d6 урона ядом.


.. _ancestry-feat--Nagaji--Guarded-Thoughts:

Защищенные мысли (`Guarded Thoughts <https://2e.aonprd.com/Feats.aspx?ID=3992>`_) / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 50

----------

Ваш разум, подобно разуму :doc:`темной наги </creatures/bestiary/Naga/Dark-Naga>`, пресекает попытки прочесть ваши мысли.
Любой эффект, который именно пытается прочитать ваш разум, чтобы удачно получить информацию, должен совершить успешную проверку :ref:`противодействия <ch9--Counteracting>` против вашего КС класса или КС заклинания, в зависимости от того, что больше; в противном случае он не получает никакой информации.
Уровень противодействия равен половине вашего уровня, округленной до большего.


.. _ancestry-feat--Nagaji--Serpentcoil-Slam:

Змеиный бросок (`Serpentcoil Slam <https://2e.aonprd.com/Feats.aspx?ID=3993>`_) |д-1| / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Частота**: раз в минуту

**Источник**: Lost Omens: Impossible Lands pg. 50

----------

Древнее и легендарное соперничество вашего народа с :doc:`гарудами </creatures/bestiary/Garuda>`, заставило вас разработать специальные приемы против летающих врагов.
Нанесите :ref:`action--Strike` ближнего боя по летающему существу, которое больше вас вплоть до 1 размера; при попадании, вы используете свою шею или кольца, чтобы ударить существо о землю.
В дополнение к обычным эффектам вашего Удара, существо перемещается в ближайшее незанятое наземное пространство рядом с вами и не может :ref:`action--Fly`, :ref:`левитировать <spell--l--Levitate>` или иным образом покидать землю в течение 1 раунда.
При критическом попадании, оно не может Летать, *левитировать* или иным образом покидать землю в течение 1 минуты.


.. _ancestry-feat--Nagaji--Serpentine-Swimmer:

Змеиный пловец (`Serpentine Swimmer <https://2e.aonprd.com/Feats.aspx?ID=3994>`_) / 9
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Предварительные условия**: :ref:`ancestry-feat--Nagaji--Water-Nagaji`

**Источник**: Lost Omens: Impossible Lands pg. 50

----------

Во время плавания вы извиваете тело змеевидными движениями, что значительно увеличивает скорость передвижения в воде.
Ваша Скорость плавания увеличивается с 10 до 25 футов.





13-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Nagaji--Disruptive-Stare:

Прерывающий взгляд (`Disruptive Stare <https://2e.aonprd.com/Feats.aspx?ID=3995>`_) |д-р| / 13
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- визуальный
- ментальный
- нагаджи

**Триггер**: Существо в пределах 30 футов пытается :ref:`Сотворить заклинание (Cast a Spell) <action--Cast-a-Spell>` с признаком :t_concentrate:`концентрации`

**Источник**: Lost Omens: Impossible Lands pg. 51

----------

Ваш холодный взгляд может превратить кровь врага в лед.
Спровоцировавшее существо должно совершить спасбросок Воли против вашего КС класса или КС заклинания, в зависимости от того, что больше.
После этого оно временно иммунно на 24 часа.

| **Провал**: Спровоцировавшее заклинание получает штраф состояния -2 к своим броскам атак и КС.
| **Критический провал**: Спровоцировавшее заклинание прервано.


.. _ancestry-feat--Nagaji--Spell-Expertise:

Эксперт заклинаний нагаджи (`Nagaji Spell Expertise <https://2e.aonprd.com/Feats.aspx?ID=3996>`_) / 13
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Предварительные условия**: :ref:`ancestry-feat--Nagaji--Spell Mysteries`

**Источник**: Lost Omens: Impossible Lands pg. 51

----------

Ваше магическое мастерство может соперничать с опытными нагами-заклинателями.
Во время своих ежедневных приготовлений выберите :ref:`spell--b--Blink`, :ref:`spell--c--Control-Water` или :ref:`spell--s--Subconscious-Suggestion`.
Вы можете раз в день творить его как врожденное оккультное заклинание 5-го уровня.
Вы становитесь экспертом КС оккультных заклинаний и бросков атак оккультными заклинаниями.


.. _ancestry-feat--Nagaji--Pit-of-Snakes:

Яма со змеями (`Pit of Snakes <https://2e.aonprd.com/Feats.aspx?ID=3997>`_) |д-3| / 13
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- оккультный
- воплощение
- воздействие
- концентрация
- нагаджи

**Частота**: раз в день

**Источник**: Lost Omens: Impossible Lands pg. 51

----------

Обычные змеи повинуются вашему призыву.
Поднимая свою руку, вы призываете извивающуюся массу змей, появляющихся из земли 20-футовым взрывом в пределах 120 футов.
Змеи появляются на всех наземных квадратах в этой области и остаются там в течение 1 минуты.
Когда вы используете "Яму со змеями", все существа в этой области должны совершить спасбросок Стойкости против вашего КС класса или КС заклинания, в зависимости от того, что больше.
При провале, существо будет :c_grabbed:`схвачено` змеей и получает 3d6 дробящего урона.
Каждый раз, когда существо заканчивает свой ход в этой области, змеи пытаются Схватить это существо, если оно еще не :c_grabbed:`схвачено`.
Любое уже :c_grabbed:`схваченное` существо получает 2d6 дробящего урона.

КС :ref:`action--Escape` от змей равен вашему КС класса или КС заклинания, в зависимости от того, что больше.
Существо может атаковать змею, пытаясь освободиться от ее хватки.
КБ змеи равен вашему КС класса или КС заклинания, в зависимости от того, что больше, и она уничтожается, если получает 12 или более урона.
Даже если конкретная змея уничтожена, другие змеи продолжают бродить в этой области до окончания продолжительности эффекта.
Вы можете :ref:`action--Dismiss` этот эффект.





17-й уровень
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _ancestry-feat--Nagaji--Breath-of-Calamity:

Дыхание катастрофы (`Breath of Calamity <https://2e.aonprd.com/Feats.aspx?ID=3998>`_) / 17
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 51

----------

Ваш рев сотрясает землю и раскалывает небеса.
Вы можете раз в день творить :ref:`spell--c--Chain-Lightning` как врожденное оккультное заклинание 7-го уровня.
Любое существо, критически провалившее спасбросок против этого заклинания, :c_blinded:`слепнет` и :c_deafened:`глохнет` на 1 раунд.


.. _ancestry-feat--Nagaji--Prismatic-Scales:

Призматическая чешуя (`Prismatic Scales <https://2e.aonprd.com/Feats.aspx?ID=3999>`_) / 17
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

- нагаджи

**Источник**: Lost Omens: Impossible Lands pg. 51

----------

Вы можете заставить свою чешую светиться сверкающими, многоцветными красками, защищая себя от энергии и ослепляя врагов.
Вы можете раз в день творить :ref:`spell--Prismatic-Armor` как врожденное оккультное заклинание, только заклинание изменяет окраску вашей чешуи, а не облачает вас в доспех.
Это различие лишь косметическое, и заклинание имеет те же эффекты, что и обычное.





.. include:: /helpers/actions.rst